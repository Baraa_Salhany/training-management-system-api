import * as paginate from 'mongoose-paginate-v2';
import { Schema, model, Types, PaginateModel } from 'mongoose';

type ObjectId = Types.ObjectId;

interface ISubject {
  _id: ObjectId;
  subject_name: string;
  stream: string;
  created_by: string;
  last_update_by?: string;
  created_at?: Date;
  updated_at?: Date;
}

const subjectSchema = new Schema<ISubject>(
  {
    subject_name: {
      type: String,
      required: true,
      index: {
        unique: true,
      },
    },
    stream: { type: String, required: true },
    created_by: { type: String, required: true },
    last_update_by: { type: String },
  },
  {
    timestamps: { createdAt: 'created_at', updatedAt: 'updated_at' },
    collation: { locale: 'en', strength: 2 },
  }
);
subjectSchema.plugin(paginate);

export const Subject = model<ISubject, PaginateModel<ISubject>>(
  'Subject',
  subjectSchema
);
