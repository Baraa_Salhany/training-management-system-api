import { Schema, model, Types } from 'mongoose';

import Constants from '../helpers/constants';

type ObjectId = Types.ObjectId;
const ObjectIdSchema = Schema.Types.ObjectId;

interface ICourse {
  _id: ObjectId;
  course_name: string;
  type: string;
  subjects: Schema.Types.Mixed[];
  created_by: string;
  last_update_by?: string;
  created_at?: Date;
  updated_at?: Date;
}

const courseSchema = new Schema<ICourse>(
  {
    course_name: { type: String, required: true, unique: true },
    type: {
      type: String,
      required: true,
      enum: Constants.COURSE_TYPES_LIST,
    },
    subjects: [
      {
        _id: {
          type: ObjectIdSchema,
          ref: 'Subject',
        },
        subject_name: { type: String, required: true },
        stream: { type: String, required: true },
      },
    ],
    created_by: { type: String, required: true },
    last_update_by: { type: String },
  },
  {
    timestamps: { createdAt: 'created_at', updatedAt: 'updated_at' },
    collation: { locale: 'en', strength: 2 },
  }
);

export const Course = model<ICourse>('Course', courseSchema);
