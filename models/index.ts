import { connect } from 'mongoose';

import { User, IUser } from './user.model';
import { Subject } from './subject.model';
import { Course } from './course.model';

import Constants from '../helpers/constants';
import { fillDataInDB } from '../controllers/helper.controller';

let connectionUrl = 'mongodb://';

// Connectiong to DB with credentials (user & passward)
if (
  Constants.TRUTHNESS_LIST.includes(
    process.env.CONNECT_DB_WITH_CREDENTIALS || ''
  )
) {
  connectionUrl +=
    process.env.MONGO_USER + ':' + process.env.MONGO_PASSWORD + '@';
}

connectionUrl +=
  process.env.MONGO_IP +
  ':' +
  process.env.MONGO_PORT +
  '/' +
  process.env.MONGO_DB;

/**
 * Connect to mongodb
 */
async function connectToDB() {
  try {
    await connect(connectionUrl);
    if (process.env.MODE == 'FIRST_RUN') {
      await fillDataInDB();
    }
  } catch (error) {
    console.error('MongoDB => Cannot connect to the database!', error);
    process.exit();
  }
}

export { connectToDB, User, IUser, Subject, Course };
