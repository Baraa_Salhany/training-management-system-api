import { Schema, model, Types } from 'mongoose';

import Constants from '../helpers/constants';

type ObjectId = Types.ObjectId;

export interface IUser {
  _id: ObjectId;
  email: string;
  username: string;
  first_name: string;
  last_name: string;
  email_verified?: boolean;
  email_token: string;
  password: string;
  role?: string;
  created_at?: Date;
  updated_at?: Date;
}

const userSchema = new Schema<IUser>(
  {
    email: { type: String, required: true, unique: true },
    username: { type: String, required: true, unique: true },
    first_name: { type: String, required: true },
    last_name: { type: String, required: true },
    email_verified: { type: Boolean, required: true, default: false },
    email_token: { type: String, required: true },
    password: { type: String, required: true },
    role: {
      type: String,
      required: true,
      enum: Constants.ROLES_LIST,
      default: Constants.ROLES.USER,
    },
  },
  {
    timestamps: { createdAt: 'created_at', updatedAt: 'updated_at' },
    collation: { locale: 'en', strength: 2 },
  }
);

export const User = model<IUser>('User', userSchema);
