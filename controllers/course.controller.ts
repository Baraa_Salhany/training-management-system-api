import { Types } from 'mongoose';

import Constants from '../helpers/constants';
import { Request, Response } from '../helpers/interfaces';
import { Course, Subject } from '../models';
import {
  logErrorAndFillResponse,
  checkRequiredFieldsInObject,
  trimValuesOfFieldsInObject,
  checkTypeOfFields,
} from './helper.controller';

/**
 * Create new course
 * @param req  Express Request
 * @param res Express Response
 */
export async function addCourse(req: Request, res: Response) {
  try {
    if (!req.body.course) {
      return res.status(400).send({
        error_message: 'Course informations are missing!',
      });
    }

    const requiredStringFields = ['course_name', 'type'];

    const missing: string[] = [];
    checkRequiredFieldsInObject(requiredStringFields, req.body.course, missing);

    if (!missing.length) {
      const invalidStringList: string[] = [];
      checkTypeOfFields(
        requiredStringFields,
        req.body.course,
        Constants.TYPES.STRING,
        invalidStringList
      );

      if (invalidStringList.length == 1) {
        return res.status(400).send({
          error_message: `Invalid ${invalidStringList[0]}!`,
        });
      }

      if (invalidStringList.length > 1) {
        return res.status(400).send({
          error_message: `The following fields have invalid values: ${invalidStringList}`,
        });
      }

      trimValuesOfFieldsInObject(requiredStringFields, req.body.course);
      checkRequiredFieldsInObject(
        requiredStringFields,
        req.body.course,
        missing
      );
    }

    if (!Constants.COURSE_TYPES_LIST.includes(req.body.course.type)) {
      return res.status(400).send({
        error_message: `Invalid Course Type!`,
      });
    }

    const subjectsIds = req.body.course.subjects;

    if (!subjectsIds) {
      missing.push('Subjects');
    }

    if (missing.length == 1) {
      return res.status(400).send({
        error_message: `${missing[0]} is missing!`,
      });
    }

    if (missing.length > 1) {
      return res.status(400).send({
        error_message: `The following fields are missing: ${missing}`,
      });
    }

    if (!Array.isArray(subjectsIds)) {
      return res.status(400).send({
        error_message: `Invalid Subjects!`,
      });
    }

    if (!subjectsIds.length) {
      return res.status(400).send({
        error_message: `Subjects array must not be empty.`,
      });
    }

    const courseWithSameName = await Course.exists({
      course_name: req.body.course.course_name,
    });

    if (courseWithSameName) {
      return res.status(400).send({
        error_message: `There is already a course with this name.`,
      });
    }

    for (const id of subjectsIds) {
      if (!Types.ObjectId.isValid(id)) {
        return res.status(400).send({
          error_message: `Invalid objectId "${id}" in subjects array.`,
        });
      }
    }

    const retrievedSubjects = await Subject.find(
      {
        _id: {
          $in: subjectsIds,
        },
      },
      Constants.SELECTED_FIELDS_OF_SUBJECT
    );

    if (!retrievedSubjects.length) {
      return res.status(400).send({
        error_message: `Please provide valid subject ids.`,
      });
    }

    await Course.create({
      course_name: req.body.course.course_name,
      type: req.body.course.type,
      subjects: retrievedSubjects,
      created_by: req.userDetails?.username,
    });

    return res.send({ message: 'Course created successfully.' });
  } catch (error) {
    logErrorAndFillResponse(res, 'addCourse', error);
  }
}

/**
 * Get all courses
 * @param req  Express Request
 * @param res Express Response
 */
export async function getCourses(req: Request, res: Response) {
  try {
    const courses = await Course.find({}, Constants.SELECTED_FIELDS_OF_COURSE);
    return res.send({ courses });
  } catch (error) {
    logErrorAndFillResponse(res, 'getCourses', error);
  }
}

/**
 * Get all courses filtered by subject id
 * @param req  Express Request
 * @param res Express Response
 */
export async function getCoursesFilteredBySubject(req: Request, res: Response) {
  try {
    const subjectId = req.query.subject_id as string;
    if (!subjectId) {
      return res.status(400).send({ error_message: 'Subject id is missing!' });
    }

    if (!Types.ObjectId.isValid(subjectId)) {
      return res.status(400).send({
        error_message: `Invalid subject id "${subjectId}".`,
      });
    }

    const subject = await Subject.findById(subjectId);

    if (!subject) {
      return res.status(400).send({
        error_message: `No subject with this id "${subjectId}".`,
      });
    }

    const courses = await Course.find(
      { 'subjects._id': subjectId },
      Constants.SELECTED_FIELDS_OF_COURSE
    );

    return res.send({ courses });
  } catch (error) {
    logErrorAndFillResponse(res, 'getCoursesFilteredBySubject', error);
  }
}

/**
 * Get all courses filtered by stream
 * @param req  Express Request
 * @param res Express Response
 */
export async function getCoursesFilteredByStream(req: Request, res: Response) {
  try {
    let stream = req.query.stream as string;
    if (stream) {
      stream = stream.trim();
    }
    if (!stream) {
      return res.status(400).send({ error_message: 'Strema is missing!' });
    }

    const courses = await Course.find(
      { 'subjects.stream': stream },
      Constants.SELECTED_FIELDS_OF_COURSE
    );

    return res.send({ courses });
  } catch (error) {
    logErrorAndFillResponse(res, 'getCoursesFilteredByStream', error);
  }
}

/**
 * Get all courses filtered by type
 * @param req  Express Request
 * @param res Express Response
 */
export async function getCoursesFilteredByType(req: Request, res: Response) {
  try {
    let type = req.query.type as string;
    if (type) {
      type = type.trim();
    }
    if (!type) {
      return res.status(400).send({ error_message: 'Type is missing!' });
    }

    const typeAny: any = type;

    if (!Constants.COURSE_TYPES_LIST.includes(typeAny)) {
      return res.status(400).send({ error_message: 'Invalid type!' });
    }

    const courses = await Course.find(
      { type },
      Constants.SELECTED_FIELDS_OF_COURSE
    );

    return res.send({ courses });
  } catch (error) {
    logErrorAndFillResponse(res, 'getCoursesFilteredByType', error);
  }
}
