import * as bcrypt from 'bcryptjs';
import * as jwt from 'jsonwebtoken';

import Constants from '../helpers/constants';
import { User } from '../models';
import { Request, Response, NextFunction } from '../helpers/interfaces';
import {
  checkRequiredFieldsInObject,
  logErrorAndFillResponse,
  checkTypeOfFields,
  trimValuesOfFieldsInObject,
} from './helper.controller';

const JWT_SECRET = process.env.JWT_SECRET || 'default secret';

/**
 * Check that all required fields for registeration are exist with valid values
 * @param req  Express Request
 * @param res Express Response
 * @param next Next handler/middleware (Express Routing)
 */
export async function checkRgisterationRequiredFileds(
  req: Request,
  res: Response,
  next: NextFunction
) {
  try {
    if (!req.body.user) {
      return res.status(400).send({
        error_message: 'User details are missing!',
      });
    }

    const requiredStringFields = [
      'email',
      'username',
      'first_name',
      'last_name',
      'password',
    ];

    const missing: string[] = [];
    checkRequiredFieldsInObject(requiredStringFields, req.body.user, missing);

    if (!missing.length) {
      const invalidStringList: string[] = [];
      checkTypeOfFields(
        requiredStringFields,
        req.body.user,
        Constants.TYPES.STRING,
        invalidStringList
      );

      if (invalidStringList.length == 1) {
        return res.status(400).send({
          error_message: `Invalid ${invalidStringList[0]}!`,
        });
      }

      if (invalidStringList.length > 1) {
        return res.status(400).send({
          error_message: `The following fields have invalid values: ${invalidStringList}`,
        });
      }

      trimValuesOfFieldsInObject(requiredStringFields, req.body.user);
      checkRequiredFieldsInObject(requiredStringFields, req.body.user, missing);
    }

    if (missing.length == 1) {
      return res.status(400).send({
        error_message: `${missing[0]} is missing!`,
      });
    }

    if (missing.length > 1) {
      return res.status(400).send({
        error_message: `The following fields are missing: ${missing}`,
      });
    }

    if (!Constants.EMAIL_REGEX.test(req.body.user.email)) {
      return res.status(400).send({
        error_message: `Invalid email!`,
      });
    }

    next();
  } catch (error) {
    logErrorAndFillResponse(res, 'checkRgisterationRequiredFileds', error);
  }
}

/**
 * Check if the username already exists or not
 * @param req  Express Request
 * @param res Express Response
 * @param next Next handler/middleware (Express Routing)
 */
export async function checkDuplicateUsername(
  req: Request,
  res: Response,
  next: NextFunction
) {
  try {
    // No need to convert to lower case after adding collation with strength 2 on schema level
    // req.body.user.username = req.body.user.username.toLowerCase();
    const user = await User.findOne({
      username: req.body.user.username,
    });

    if (user) {
      return res.status(400).send({
        error_message: 'This username already used!',
      });
    }

    next();
  } catch (error) {
    logErrorAndFillResponse(res, 'checkDuplicateUsername', error);
  }
}

/**
 * Check if the email already exists or not
 * @param req  Express Request
 * @param res Express Response
 * @param next Next handler/middleware (Express Routing)
 * @returns
 */
export async function checkDuplicateEmail(
  req: Request,
  res: Response,
  next: NextFunction
) {
  try {
    // No need to convert to lower case after adding collation with strength 2 on schema level
    // req.body.user.email = req.body.user.email.toLowerCase();
    const user = await User.findOne({
      email: req.body.user.email,
    });
    if (user) {
      return res.status(400).send({
        error_message: 'This email already registered!',
      });
    }

    next();
  } catch (error) {
    logErrorAndFillResponse(res, 'checkDuplicateEmail', error);
  }
}

/**
 * Register new user
 * @param req  Express Request
 * @param res Express Response
 * @param next Next handler/middleware (Express Routing)
 */
export async function register(
  req: Request,
  res: Response,
  next: NextFunction
) {
  try {
    // Generate email token to be used in email verification
    const emailToken = jwt.sign({ email: req.body.user.email }, JWT_SECRET, {
      expiresIn: process.env.EMAIL_VERIFICATION_TOKEN_VALIDITY,
    });

    // Save User to Database
    await User.create({
      email: req.body.user.email,
      username: req.body.user.username,
      first_name: req.body.user.first_name,
      last_name: req.body.user.last_name,
      email_token: emailToken,
      password: bcrypt.hashSync(req.body.user.password),
    });

    // Add email token to the request to be used in sending verification email
    req.emailToken = emailToken;

    next();
  } catch (error) {
    logErrorAndFillResponse(res, 'register', error);
  }
}

/**
 * Verify email of new registered user
 * @param req  Express Request
 * @param res Express Response
 */
export async function verifyEmail(req: Request, res: Response) {
  try {
    const emailToken = req.query.email_token as string;
    if (!req.query.email_token) {
      return res.status(400).send({ error_message: 'Email token is missing!' });
    }

    const user = await User.findOne({
      email_token: emailToken,
      email_verified: false,
    });

    if (!user) {
      return res.status(400).send({ error_message: 'Invalid link!' });
    }

    try {
      await jwt.verify(emailToken, JWT_SECRET);
    } catch (error) {
      return res.status(400).send({
        error_message: 'Invalid link!',
      });
    }
    await User.updateOne({ email_token: emailToken }, { email_verified: true });
    return res.status(200).send({
      message: 'Your email has been verified successfully!',
    });
  } catch (error) {
    logErrorAndFillResponse(res, 'verifyEmail', error);
  }
}

/**
 * User login
 * @param req  Express Request
 * @param res Express Response
 * @returns Access token with some user's details
 */
export async function login(req: Request, res: Response) {
  try {
    if (!req.body.user) {
      return res
        .status(400)
        .send({ error_message: `Credentials are missing!` });
    }

    const requiredFields = ['username', 'password'];

    const missing: string[] = [];
    checkRequiredFieldsInObject(requiredFields, req.body.user, missing);

    if (!missing.length) {
      trimValuesOfFieldsInObject(requiredFields, req.body.user);
      checkRequiredFieldsInObject(requiredFields, req.body.user, missing);
    }

    if (missing.length == 1) {
      return res.status(400).send({
        error_message: `${missing[0]} is missing!`,
      });
    }

    if (missing.length > 1) {
      return res.status(400).send({
        error_message: `Username and password are missing!`,
      });
    }

    // No need to convert to lower case after adding collation with strength 2 on schema level
    // const username = req.body.user.username.toLowerCase();
    const username = req.body.user.username;
    const user = await User.findOne({
      username,
    });
    if (!user) {
      return res.status(400).send({ error_message: 'Wrong username!' });
    }

    const passwordIsValid = bcrypt.compareSync(
      req.body.user.password,
      user.password
    );

    if (!passwordIsValid) {
      return res.status(401).send({
        error_message: 'Wrong Password!',
      });
    }

    if (!user.email_verified) {
      return res.status(401).send({
        error_message: 'Please Verify Your Email First',
      });
    }

    const token = await jwt.sign({ id: user.id }, JWT_SECRET, {
      expiresIn: process.env.SESSION_ACTIVE_TIME, // defining how long the JWT token will remain valid
    });

    res.status(200).send({
      id: user.id,
      username: user.username,
      email: user.email,
      role: user.role,
      accessToken: token,
    });
  } catch (error) {
    logErrorAndFillResponse(res, 'login', error);
  }
}

/**
 * Extract JWT from the header
 * @param req  Express Request
 * @returns JWT
 */
function extractJWT(req: Request) {
  // Get the Authorization header from the request
  let authorization = req.get('Authorization');

  if (!authorization) return null;

  // Header value must start with 'Bearer '
  if (authorization.length <= 7) return null;
  let prefix = authorization.substring(0, 7);
  if (prefix != 'Bearer ') return null;

  // Extract the token (after 'Bearer ')
  let token = authorization.substring(7);
  return token;
}

/**
 * Verify access token of user
 * @param req  Express Request
 * @param res Express Response
 * @param next Next handler/middleware (Express Routing)
 */
export async function verifyToken(
  req: Request,
  res: Response,
  next: NextFunction
) {
  try {
    if (!req.headers) {
      return res.status(401).send({
        error_message: 'Unauthorized',
      });
    }

    // Get token from the header
    const token = extractJWT(req);

    // If there is no token in the header
    if (!token) {
      return res.status(401).send({
        error_message: 'Unauthorized',
      });
    }

    const decoded = (await jwt.verify(token, JWT_SECRET)) as jwt.JwtPayload;

    // User id that is returned after decryption
    const decodedId = decoded.id;

    const user = await User.findById(
      decodedId,
      Constants.SELECTED_FIELDS_OF_USER
    );

    if (!user) {
      return res.status(401).send({
        error_message: 'Unauthorized',
      });
    }

    // Add user detail to the request for later use
    req.userDetails = user;

    next();
  } catch (error) {
    console.error(`### Error inside verifyToken function: ${error}`);
    //token is invalid
    return res.status(401).send({
      error_message: 'Unauthorized',
    });
  }
}

/**
 * Check that user has 'ADMIN' role
 * Note: This function can just be used after the verifyToken middleware, because it is
 * assumes that userDetails object already added to the request
 * @param req  Express Request
 * @param res Express Response
 * @param next Next handler/middleware (Express Routing)
 */
export async function isAdmin(req: Request, res: Response, next: NextFunction) {
  try {
    if (req.userDetails!.role !== Constants.ROLES.ADMIN) {
      return res.status(401).send({
        error_message: 'Unauthorized',
      });
    }
    next();
  } catch (error) {
    logErrorAndFillResponse(res, 'isAdmin', error);
  }
}
