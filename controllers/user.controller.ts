import * as bcrypt from 'bcryptjs';

import Constants from '../helpers/constants';
import { Request, Response, UpdatableUserFields } from '../helpers/interfaces';
import { User } from '../models';
import {
  logErrorAndFillResponse,
  checkAndTrimStringFieldsIfExist,
  trimValuesOfFieldsInObject,
  checkTypeOfFields,
} from './helper.controller';

/**
 * Get user's details
 * @param req  Express Request
 * @param res Express Response
 * @returns User details object
 */
export async function getUser(req: Request, res: Response) {
  try {
    const userDetails = req.userDetails!;
    return res.send({
      user: {
        _id: userDetails._id,
        email: userDetails.email,
        username: userDetails.username,
        first_name: userDetails.first_name,
        last_name: userDetails.last_name,
        email_verified: userDetails.email_verified,
        role: userDetails.role,
      },
    });
  } catch (error) {
    logErrorAndFillResponse(res, 'getUser', error);
  }
}

/**
 * Update user's details
 * @param req  Express Request
 * @param res Express Response
 * @returns Success message
 */
export async function updateUser(req: Request, res: Response) {
  try {
    if (!req.body.user) {
      return res.status(400).send({
        error_message: 'user informations are missing!',
      });
    }

    const updatableStringFields = ['first_name', 'last_name'];

    const invalidStringList: string[] = [];
    checkAndTrimStringFieldsIfExist(
      updatableStringFields,
      req.body.user,
      invalidStringList
    );

    if (invalidStringList.length == 1) {
      return res.status(400).send({
        error_message: `Invalid ${invalidStringList[0]}!`,
      });
    }

    if (invalidStringList.length > 1) {
      return res.status(400).send({
        error_message: `The following fields have invalid values: ${invalidStringList}`,
      });
    }

    if (!req.body.user.first_name && !req.body.user.last_name) {
      return res.status(400).send({
        error_message: `Empty data, please send some data if you want to update you profile.`,
      });
    }

    let updatedFields: UpdatableUserFields = {
      first_name: req.body.user.first_name || req.userDetails?.first_name,
      last_name: req.body.user.last_name || req.userDetails?.last_name,
    };

    await User.updateOne({ _id: req.userDetails?._id }, updatedFields);

    return res.send({
      message: 'Your information has been updated successfully!',
    });
  } catch (error) {
    logErrorAndFillResponse(res, 'updateUser', error);
  }
}

/**
 * Update user's password
 * @param req  Express Request
 * @param res Express Response
 * @returns Success message
 */
export async function changePassword(req: Request, res: Response) {
  try {
    if (!req.body.old_password || !req.body.new_password) {
      return res.status(400).send({
        error_message: 'Please send both old & new passwords".',
      });
    }

    const passwordFields = ['old_password', 'new_password'];

    const invalidStringList: string[] = [];
    checkTypeOfFields(
      passwordFields,
      req.body,
      Constants.TYPES.STRING,
      invalidStringList
    );

    if (invalidStringList.length == 1) {
      return res.status(400).send({
        error_message: `Invalid ${invalidStringList[0]}!`,
      });
    }

    if (invalidStringList.length > 1) {
      return res.status(400).send({
        error_message: `The following fields have invalid values: ${invalidStringList}`,
      });
    }

    trimValuesOfFieldsInObject(passwordFields, req.body);

    if (!req.body.old_password || !req.body.new_password) {
      return res.status(400).send({
        error_message: 'Please send both old & new passwords".',
      });
    }

    const userDetails = req.userDetails!;

    const passwordIsValid = bcrypt.compareSync(
      req.body.old_password,
      userDetails.password
    );

    if (!passwordIsValid) {
      return res.status(400).send({
        error_message: 'The old password is wrong!',
      });
    }

    await User.updateOne(
      { _id: req.userDetails?._id },
      { password: bcrypt.hashSync(req.body.new_password) }
    );

    return res.send({
      message: 'Your password has been updated successfully!',
    });
  } catch (error) {
    logErrorAndFillResponse(res, 'updateUser', error);
  }
}
