import Constants from '../helpers/constants';
import { Request, Response } from '../helpers/interfaces';
import { Subject } from '../models';
import {
  logErrorAndFillResponse,
  checkRequiredFieldsInObject,
  trimValuesOfFieldsInObject,
  checkTypeOfFields,
} from './helper.controller';

/**
 * Create new subject
 * @param req  Express Request
 * @param res Express Response
 */
export async function addSubject(req: Request, res: Response) {
  try {
    if (!req.body.subject) {
      return res.status(400).send({
        error_message: 'Subject informations are missing!',
      });
    }

    const requiredStringFields = ['subject_name', 'stream'];

    const missing: string[] = [];
    checkRequiredFieldsInObject(
      requiredStringFields,
      req.body.subject,
      missing
    );

    if (!missing.length) {
      const invalidStringList: string[] = [];
      checkTypeOfFields(
        requiredStringFields,
        req.body.subject,
        Constants.TYPES.STRING,
        invalidStringList
      );

      if (invalidStringList.length == 1) {
        return res.status(400).send({
          error_message: `Invalid ${invalidStringList[0]}!`,
        });
      }

      if (invalidStringList.length > 1) {
        return res.status(400).send({
          error_message: `The following fields have invalid values: ${invalidStringList}`,
        });
      }

      trimValuesOfFieldsInObject(requiredStringFields, req.body.subject);
      checkRequiredFieldsInObject(
        requiredStringFields,
        req.body.subject,
        missing
      );
    }

    if (missing.length == 1) {
      return res.status(400).send({
        error_message: `${missing[0]} is missing!`,
      });
    }

    if (missing.length > 1) {
      return res.status(400).send({
        error_message: `The following fields are missing: ${missing}`,
      });
    }

    const subjectWithSameName = await Subject.exists({
      subject_name: req.body.subject.subject_name,
    });

    if (subjectWithSameName) {
      return res.status(400).send({
        error_message: `There is already a subject with this name.`,
      });
    }

    await Subject.create({
      subject_name: req.body.subject.subject_name,
      stream: req.body.subject.stream,
      created_by: req.userDetails?.username,
    });

    return res.send({ message: 'Subject created successfully.' });
  } catch (error) {
    logErrorAndFillResponse(res, 'addSubject', error);
  }
}

/**
 * Get all subjects with pagination
 * @param req  Express Request
 * @param res Express Response
 */
export async function getSubjects(req: Request, res: Response) {
  try {
    let sortInt = 1;
    const sortString = req.query.sort as string;
    const pageSizeAsString = req.query.page_size as string;
    const pageNumberAsString = req.query.page_number as string;

    let pageSize = parseInt(pageSizeAsString);
    const pageNumber = parseInt(pageNumberAsString);

    if (!Number.isInteger(pageSize) || pageSize < 1) {
      pageSize = Constants.DEFAUL_PAGE_SIZE;
    }

    if (pageSize > 500) {
      return res
        .status(400)
        .send({ error_message: 'Page size limited up to 500' });
    }

    if (!Number.isInteger(pageNumber) || pageNumber < 1) {
      return res.status(400).send({ error_message: 'Invalid page number' });
    }

    if (sortString && sortString.toUpperCase() == Constants.SORT_TYPES.DSC) {
      sortInt = -1;
    }

    const subjects = await Subject.paginate(
      {},
      {
        projection: Constants.SELECTED_FIELDS_OF_SUBJECT,
        page: pageNumber,
        limit: pageSize,
        sort: {
          created_at: sortInt,
        },
      }
    );

    return res.send({ subjects });
  } catch (error) {
    logErrorAndFillResponse(res, 'getSubjects', error);
  }
}
