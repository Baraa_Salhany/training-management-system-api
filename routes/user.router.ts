import { Router } from 'express';

import * as authController from '../controllers/auth.controller';
import * as userController from '../controllers/user.controller';

const router = Router();

// Apply verifyToken middleware to all the following endpoints
router.use(authController.verifyToken);

// Get user's details
router.get('/', userController.getUser);

// Update user's profile
router.put('/', userController.updateUser);

// Update user's password
router.put('/change-password', userController.changePassword);

export default router;
