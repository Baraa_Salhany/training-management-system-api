import { Request, Response, Router } from 'express';

import authRouter from './auth.router';
import userRouter from './user.router';
import subjectRouter from './subject.router';
import courseRouter from './course.router';

const router = Router();

// status endpoint
router.get('/status', async (req: Request, res: Response) => {
  res.send({ message: 'Training Management System API is Working!' });
});

router.use('/auth', authRouter);
router.use('/user', userRouter);
router.use('/subjects', subjectRouter);
router.use('/courses', courseRouter);

export default router;
