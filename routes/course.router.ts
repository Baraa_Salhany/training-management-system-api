import { Router } from 'express';

import * as authController from '../controllers/auth.controller';
import * as courseController from '../controllers/course.controller';

const router = Router();

// Apply verifyToken middleware to all the following endpoints
router.use(authController.verifyToken);

// Create new course
router.post('/', authController.isAdmin, courseController.addCourse);

// Get all courses
router.get('/', courseController.getCourses);

// Get all courses filtered by subject id
router.get('/by-subject', courseController.getCoursesFilteredBySubject);

// Get all courses filtered by stream
router.get('/by-stream', courseController.getCoursesFilteredByStream);

// Get all courses filtered by type
router.get('/by-type', courseController.getCoursesFilteredByType);

export default router;
