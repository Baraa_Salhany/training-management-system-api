import { Router } from 'express';

import * as authController from '../controllers/auth.controller';
import * as subjectController from '../controllers/subject.controller';

const router = Router();

// Apply verifyToken middleware to all the following endpoints
router.use(authController.verifyToken);

// Create new subject
router.post('/', authController.isAdmin, subjectController.addSubject);

// Get all subjects with pagination
router.get('/', subjectController.getSubjects);

export default router;
