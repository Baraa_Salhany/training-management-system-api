import { Router } from 'express';

import * as authController from '../controllers/auth.controller';
import * as communicationController from '../controllers/communication.controller';

const router = Router();

// Register new user
router.post(
  '/register',
  [
    authController.checkRgisterationRequiredFileds, // check that all required fields for registeration are exist with valid values
    authController.checkDuplicateUsername, // check if the username already exists or not
    authController.checkDuplicateEmail, // check if the email already exists or not
  ],

  authController.register,
  communicationController.sendVerificationEmail // Send email to user to verify
);

// Login endpoint
router.post('/login', authController.login);

// Verify email endpoint
router.get('/verify-email', authController.verifyEmail);

export default router;
