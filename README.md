- This repo has the code of Training Management System API
- Required Node version: v14.6.0
- This project connect to MongoDB (You can download MongoDB from this link: https://www.mongodb.com/try/download/community).
- To run the project locally, you should follow the following steps:
  1. Clone the repo.
  2. Install the required packages by running "npm i" command.
  3. Create .env file in the root folder of the project.
  4. Copy the content of .env.example file to .env file
  5. Set value for "SENDGRID_API_KEY" environment variable to be able to send emails to new registered user to verify his email. (if you don't have a sendgrid account, you can sign up for free using the link: https://signup.sendgrid.com/)
  6. For the first run use this command: "MODE=FIRST_RUN npm start" which will insert some data to the DB.
  7. For the subsequent runs, use the command: "npm start"
- To test the API, use the swagger documentation on this link: http://localhost:5001/api-docs

Notes:
Since some endpoints accessible just for users with "ADMIN" role, you can use the following credentials to test them:
"username": "baraa"
"password": "test123"
