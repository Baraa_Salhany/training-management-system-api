import { Request, Response, NextFunction } from 'express';
import { IUser } from '../models';

declare module 'express-serve-static-core' {
  interface Request {
    userDetails?: IUser;
    emailToken?: string;
  }
}

export { Request, Response, NextFunction };

export interface UpdatableUserFields {
  first_name: string;
  last_name: string;
}
