export default class Constants {
  /* constants: Integers */
  static DEFAUL_PAGE_SIZE = 10;
  /* constants: Integers */

  /* constants: Strings */
  static EMAIL_FROM = 'Training <baraa.salhany@smartretail.co>';
  static EMAIL_REGEX =
    /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  static SELECTED_FIELDS_OF_USER =
    'email username first_name last_name email_verified role password';
  static SELECTED_FIELDS_OF_SUBJECT = 'subject_name stream';
  static SELECTED_FIELDS_OF_COURSE = 'course_name type subjects';
  /* constants: Strings */

  /* constants: Objects */
  static ROLES = {
    ADMIN: 'ADMIN',
    USER: 'USER',
  } as const;

  static SORT_TYPES = {
    ASC: 'ASC',
    DSC: 'DSC',
  } as const;

  static COURSE_TYPES = {
    BASIC: 'Basic',
    DETAILED: 'Detailed',
  } as const;

  static TYPES = {
    STRING: 'string',
  };
  /* constants: Objects */

  /* constants: Arrays */
  static ROLES_LIST = Object.values(Constants.ROLES);
  static COURSE_TYPES_LIST = Object.values(Constants.COURSE_TYPES);
  static TRUTHNESS_LIST = ['True', 'true', '1'];
  static REQUIRED_ENVIRONMENT_VARIABLES = [
    'PORT',
    'SERVER_URL',
    'CONNECT_DB_WITH_CREDENTIALS',
    'MONGO_IP',
    'MONGO_PORT',
    'MONGO_DB',
    'JWT_SECRET',
    'EMAIL_VERIFICATION_TOKEN_VALIDITY',
    'SESSION_ACTIVE_TIME',
    'SENDGRID_API_KEY',
  ];
  /* constants: Arrays */
}
