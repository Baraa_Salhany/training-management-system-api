import * as dotenv from 'dotenv';
import * as express from 'express';
import * as bodyParser from 'body-parser';
import * as cors from 'cors';
import * as swaggerUi from 'swagger-ui-express';
import * as swaggerDocument from './swagger.json';

dotenv.config({
  path: `.env`,
});

import routes from './routes';
import { connectToDB } from './models';
import { checkMissingEnvironmentVariables } from './controllers/helper.controller';

// Check if any of required environment variables is missing
checkMissingEnvironmentVariables();

const app = express();

app.use(cors());
app.use(bodyParser.json());

// routes
app.use('/api', routes);

// Documentation
app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerDocument));

connectToDB().then(() => {
  console.log('MongoDB => Connected to the database!');
});

// set port then listen for requests
const PORT = process.env.PORT || 5001;
try {
  app.listen(PORT, () => {
    console.log(`Express => Server is running on port ${PORT}.`);
  });
} catch (error) {
  console.error(`Error occured while trying to run the server: ${error}`);
}
